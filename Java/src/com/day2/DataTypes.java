package com.day2;

public class DataTypes {
	byte a1;
	short b1;
	int c1;
	long d1;
	boolean ispresent1;
	float f1;
	double h1;
	char g1;

	public static void main(String[] args) 
	{
		local();
	}

	public static void local()
	{
		System.out.println("Local Variables Assignment");
		byte a=1;
		short b=2;
		int c=12;
		long d= 868768766878686l;
		boolean ispresent=true;
		float f=3.144f;
		double h= 868.76876d;
		char g='A';
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		System.out.println(f);
		System.out.println(g);
		System.out.println(h);
		System.out.println(ispresent);
	}
	
}
